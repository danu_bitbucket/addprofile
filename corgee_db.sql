/*
SQLyog Community v12.2.2 (64 bit)
MySQL - 5.7.31 : Database - corgee_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`corgee_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `corgee_db`;

/*Table structure for table `transaction` */

DROP TABLE IF EXISTS `transaction`;

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(200) DEFAULT NULL,
  `amount` float DEFAULT '0',
  `typetrans` varchar(10) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `transaction` */

insert  into `transaction`(`id`,`description`,`amount`,`typetrans`) values 
(2,'UPDATE_456',500,'expense'),
(3,'UPDATE_456',500,'expense'),
(4,'Des_456',500,'expense'),
(5,'Des_456',500,'expense'),
(6,'Des_456',500,'expense'),
(7,'Des_456',500,'expense'),
(8,'Des_456',500,'expense'),
(9,'Des_456',500,'expense'),
(10,'Des_456',500,'expense'),
(11,'Des_456',500,'expense'),
(12,'Des_456',500,'expense'),
(13,'Des_456',500,'expense'),
(14,'XXX_456',500,'expense');

/*Table structure for table `user_info` */

DROP TABLE IF EXISTS `user_info`;

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `user_info` */

insert  into `user_info`(`id`,`firstname`,`lastname`,`email`,`password`) values 
(1,'Danu','Sukmawijaya','danu.sukma@gmail.com','$2b$12$ch05f0K/jV2f9CpExWX.l.OcIY.t1afTJ7chGDI603lf2Mh8gPY9W');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
