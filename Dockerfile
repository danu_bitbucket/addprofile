FROM python:3.8.5
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install fastapi uvicorn

COPY requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt

COPY ./app /app
EXPOSE 8001
WORKDIR /app

CMD ["uvicorn", "main:app", "--reload", "--workers", "1", "--host", "0.0.0.0", "--port", "8001"]

