import uvicorn
from sqlalchemy.orm import Session
from fastapi import Depends, FastAPI, HTTPException
from sql_app import models, schemas, crud
from sql_app.database import engine, SessionLocal
import re

models.Base.metadata.create_all(bind=engine)

ACCESS_TOKEN_EXPIRE_MINUTES = 30

app = FastAPI()


# Dependency


def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


# Dependency
@app.get("/")
def read_root():
    return {"Hello": "Corgee"}


# USER CONTROLLER

@app.post("/register", response_model=schemas.UserInfo, tags=["Register User"])
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):

    _email = email_validation(user.email);

    if _email == False:
        raise HTTPException(status_code=400, detail="Email not valid!")
    
    if user.password== "":
        raise HTTPException(status_code=400, detail="Password not valid!")

    if user.password != user.repeat_password:
        raise HTTPException(status_code=400, detail="Password not match!")

    db_user = crud.get_user_by_username(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return crud.create_user(db=db, user=user)


@app.post("/authenticate", response_model=schemas.Token, tags=["Authenticate User"])
def authenticate_user(user: schemas.UserAuthenticate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_username(db, email=user.email)
    if db_user is None:
        raise HTTPException(status_code=400, detail="Email not existed")
    else:
        is_password_correct = crud.check_username_password(db, user)
        if is_password_correct is False:
            raise HTTPException(status_code=400, detail="Password is not correct")
        else:
            from datetime import timedelta
            access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
            from sql_app.app_utils import create_access_token
            access_token = create_access_token(
                data={"sub": user.email}, expires_delta=access_token_expires)
            return {"access_token": access_token, "token_type": "Bearer"}

def email_validation(email):    
    # pass the regular expression
    # and the string into the fullmatch() method
    _result = False
    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

    if(re.fullmatch(regex, email)):
        _result = True

    return _result
 
 #TRANSACTION CONTROLLER
@app.post("/transaction",  status_code=200, tags=["Create Transaction"])
def create_transaction(trans: schemas.TransactionCreate, db: Session = Depends(get_db)):
    _result =  crud.create_transaction(db=db, trans=trans)

    final = {
        "Status": "Success",
        "Message":_result
    }
    return (final)

@app.delete("/transaction",  status_code=200, tags=["Delete Transaction"])
def create_transaction(trans: schemas.TransactionDelete, db: Session = Depends(get_db)):
    _result =  crud.delete_transaction(db=db, trans=trans)

    final = {
        "Status": "Success",
        "Message":_result
    }
    return (final)

@app.put("/transaction",  status_code=200,tags=["Update Transaction"])
def create_transaction(trans: schemas.TransactionUpdate, db: Session = Depends(get_db)):
    _result =  crud.update_transaction(db=db, trans=trans)

    final = {
        "Status": "Success",
        "Message":trans
    }
    return (final)

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8081)
