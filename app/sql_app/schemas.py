from typing import List
from pydantic import BaseModel

#User Schema

class UserInfoBase(BaseModel):
    email: str


class UserCreate(UserInfoBase):
    firstname: str
    lastname: str
    password: str
    repeat_password: str


class UserAuthenticate(UserInfoBase):
    password: str


class UserInfo(UserInfoBase):
    id: int

    class Config:
        orm_mode = True


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: str = None

#Transaction Schema
class TransactionCreate(BaseModel):
    description: str
    amount: float
    typetrans: str

class TransactionUpdate(BaseModel):
    id: int
    description: str
    amount: float
    typetrans: str

class TransactionDelete(BaseModel):
    id: int

class TransactionInfo(BaseModel):
    id: int

    class Config:
        orm_mode = True