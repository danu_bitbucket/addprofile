from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


db_host = "192.168.100.12"
db_user = "userdocker"
db_pwd = "admin1234"
db_name = "corgee_db"

SQLALCHEMY_DATABASE_URL = "mysql+mysqlconnector://"+db_user+":"+db_pwd+"@" + db_host + ":3306/" + db_name

engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()





