from sqlalchemy.orm import Session
from . import models, schemas
import bcrypt


#USER CRUD
def get_user_by_username(db: Session, email: str):
    return db.query(models.UserInfo).filter(models.UserInfo.email == email).first()


def create_user(db: Session, user: schemas.UserCreate):
    hashed_password = bcrypt.hashpw(user.password.encode('utf-8'), bcrypt.gensalt())
    db_user = models.UserInfo(firstname=user.firstname, lastname=user.lastname, password=hashed_password, email=user.email)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def check_username_password(db: Session, user: schemas.UserAuthenticate):
    db_user_info: models.UserInfo = get_user_by_username(db, email=user.email)
    return bcrypt.checkpw(user.password.encode('utf-8'), db_user_info.password.encode('utf-8'))


#TRANSACTION CRUD
def create_transaction(db: Session, trans: schemas.TransactionCreate):
    db_trans = models.Transaction(description=trans.description, amount=trans.amount, typetrans=trans.typetrans)
    db.add(db_trans)
    db.commit()
    db.refresh(db_trans)
    return db_trans

def delete_transaction(db: Session, trans: schemas.TransactionDelete):
    db_trans = db.query(models.Transaction).filter(models.Transaction.id == trans.id).first()
    if db_trans:
        db.delete(db_trans)
        db.commit()

    return db_trans

def update_transaction(db: Session, trans: schemas.TransactionUpdate):
    db_trans = db.query(models.Transaction).filter(models.Transaction.id == trans.id).first()
    if db_trans:
        db_trans.description = trans.description
        db_trans.amount = trans.amount
        db_trans.typetrans = trans.typetrans
        db.add(db_trans)
        db.commit()

    return db_trans