from sqlalchemy import Column, Integer, String, DECIMAL
from sql_app.database import Base


class UserInfo(Base):
    __tablename__ = "user_info"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True)
    firstname = Column(String)
    lastname = Column(String)
    password = Column(String)
    

class Transaction(Base):
    __tablename__ = "transaction"

    id = Column(Integer, primary_key=True, index=True)
    description = Column(String())
    amount = Column(DECIMAL(13, 2))
    typetrans = Column(String)